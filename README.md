Ancestry is a program for inferring the "genetic ancestry" of an individual from their genotype or sequence data. It requires a reference set of labeled individuals, and models the test individual as a mixture of these reference sets. The underlying model is the [Pritchard, Stephens, and Donnelly STRUCTURE model](http://www.ncbi.nlm.nih.gov/pubmed/10835412). 

More detailed documentation to come. 

#Dependencies
1. [The GNU Scientific Library](http://www.gnu.org/software/gsl/)
2. [The Boost libraries](http://www.boost.org/)

#Install:

>./configure

>make

#Arguments
-i input matrix of allele counts in the reference populations

this file must be gzipped.

-g23 23andMe-style genotype file of test individual

Alternative, use -gp for a pileup file

-gp gzipped pileup file
